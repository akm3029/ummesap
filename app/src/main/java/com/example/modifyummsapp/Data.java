
package com.example.modifyummsapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("UserMaster")
    @Expose
    private UserMaster userMaster;
    @SerializedName("OfficeUser")
    @Expose
    private OfficeUser officeUser;
    @SerializedName("CrewInfo")
    @Expose
    private CrewInfo crewInfo;
    @SerializedName("VesselList")
    @Expose
    private VesselList vesselList;

    public UserMaster getUserMaster() {
        return userMaster;
    }

    public void setUserMaster(UserMaster userMaster) {
        this.userMaster = userMaster;
    }

    public OfficeUser getOfficeUser() {
        return officeUser;
    }

    public void setOfficeUser(OfficeUser officeUser) {
        this.officeUser = officeUser;
    }

    public CrewInfo getCrewInfo() {
        return crewInfo;
    }

    public void setCrewInfo(CrewInfo crewInfo) {
        this.crewInfo = crewInfo;
    }

    public VesselList getVesselList() {
        return vesselList;
    }

    public void setVesselList(VesselList vesselList) {
        this.vesselList = vesselList;
    }

}
