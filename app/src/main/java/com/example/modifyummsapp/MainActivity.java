package com.example.modifyummsapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    TabLayout tabLayout;
    ViewPager frame;
    Toolbar toolbar;
    private DrawerLayout drawerLayout;
    Fragment fragment = null;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    ActionBarDrawerToggle mDrawerToggle;
    Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         spinner=(Spinner)findViewById(R.id.spinner);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
       //toolbar.setNavigationOnClickListener(new View.OnClickListener() {
          //  @Override
          //  public void onClick(View view) {
           //     drawerLayout.openDrawer(Gravity.LEFT);
            //  Toast.makeText(getApplicationContext(), "Back clicked!",     Toast.LENGTH_SHORT).show();
           // }
      //  });

      // spinner.setPrompt("Username");
        // spinner.setOnItemClickListener((AdapterView.OnItemClickListener) this);
        List<String> cate=new ArrayList<String>();
        cate.add("item1");
        cate.add("item2");
        cate.add("item3");
        ArrayAdapter<String> data=new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,cate);
        data.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       spinner.setAdapter(data);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        frame = (ViewPager) findViewById(R.id.frame);
        tabLayout.addTab(tabLayout.newTab().setText("Defect List"));
        tabLayout.addTab(tabLayout.newTab().setText("Routine Job"));
        tabLayout.addTab(tabLayout.newTab().setText("Non-Routine/Breakdown"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPagerAdapter adapter = new ViewPagerAdapter(this,getSupportFragmentManager(), tabLayout.getTabCount());
        frame.setAdapter(adapter);

        frame.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                frame.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


//configureNavigationDrawer();


    }
   @Override
    public void onItemSelected(AdapterView<?> parent, View view, int positon, long id)
    {
        String item=parent.getItemAtPosition(positon).toString();
    }
    @Override
    public void onNothingSelected(AdapterView<?>arg0){

    }
    private void configureNavigationDrawer() {

        NavigationView navView = (NavigationView) findViewById(R.id.navigation);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Fragment f = null;
                int itemId = menuItem.getItemId();
                if (itemId == R.id.first) {
                } else if (itemId == R.id.second) {
                }
                if (f != null) {
                    drawerLayout.closeDrawers();
                    return true;
                }
                return false;
            }
        });
    }



}