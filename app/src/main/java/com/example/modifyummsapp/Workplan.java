
package com.example.modifyummsapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Workplan {

    @SerializedName("WorkPlanId")
    @Expose
    private String workPlanId;
    @SerializedName("WorkPlanNumber")
    @Expose
    private String workPlanNumber;
    @SerializedName("VesselName")
    @Expose
    private String vesselName;
    @SerializedName("DateOpen")
    @Expose
    private String dateOpen;
    @SerializedName("DateClosed")
    @Expose
    private Object dateClosed;
    @SerializedName("NumberOfJobs")
    @Expose
    private String numberOfJobs;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getWorkPlanId() {
        return workPlanId;
    }

    public void setWorkPlanId(String workPlanId) {
        this.workPlanId = workPlanId;
    }

    public String getWorkPlanNumber() {
        return workPlanNumber;
    }

    public void setWorkPlanNumber(String workPlanNumber) {
        this.workPlanNumber = workPlanNumber;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public Object getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(Object dateClosed) {
        this.dateClosed = dateClosed;
    }

    public String getNumberOfJobs() {
        return numberOfJobs;
    }

    public void setNumberOfJobs(String numberOfJobs) {
        this.numberOfJobs = numberOfJobs;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
