package com.example.modifyummsapp;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DefectFragment extends Fragment {
 /*   private DefectAdapter retrofitAdapter;
    private RecyclerView recyclerView;
    View v;

    public DefectFragment() {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v= inflater.inflate(R.layout.fragment_defect, container, false);

        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview1);

        fetchJSON();

        return v;

    }

    private void fetchJSON(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.JSONURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        Call<String> call = api.getString();

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i("Responsestring", response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i("onSuccess", response.body().toString());

                        String jsonresponse = response.body().toString();
                        writeRecycler(jsonresponse);

                    } else {
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void writeRecycler(String response){

        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);
            if(obj.optString("status").equals("true")){

                ArrayList<Defectmodel> modelRecyclerArrayList = new ArrayList<>();
                JSONArray dataArray  = obj.getJSONArray("data");

                for (int i = 0; i < dataArray.length(); i++) {

                    Defectmodel modelRecycler = new Defectmodel();
                    JSONObject dataobj = dataArray.getJSONObject(i);

                    modelRecycler.setDuedate(dataobj.getString("duedate"));
                    modelRecycler.setCode(dataobj.getString("code"));
                    modelRecycler.setDefecttext(dataobj.getString("defecttext"));
                    modelRecycler.setCorrectionaction(dataobj.getString("correctionaction"));

                    modelRecyclerArrayList.add(modelRecycler);

                }

                retrofitAdapter = new DefectAdapter(getActivity().getApplicationContext(),modelRecyclerArrayList);
                recyclerView.setAdapter(retrofitAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }*/

    View v;
    private ArrayList<Defectmodel> defectitem;
    //RecyclerView recyclerView;

   // Context context;
    Spinner spinner;

    //RecyclerView.Adapter recyclerView_Adapter;

   // RecyclerView.LayoutManager recyclerViewLayoutManager;

    /*String[] numbers = {
            "HGB/001/19",
            "HGB/001/19",
            "HGB/001/19",
            "HGB/001/19",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",

    };*/
        public DefectFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            v= inflater.inflate(R.layout.fragment_defect, container, false);
            spinner=(Spinner)v.findViewById(R.id.sp);
            //spinner.setPrompt("Select");
          /*  context = getActivity().getApplicationContext();
            recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview1);

            //Change 2 to your choice because here 2 is the number of Grid layout Columns in each row.
            recyclerViewLayoutManager = new GridLayoutManager(context, 1);

            recyclerView.setLayoutManager(recyclerViewLayoutManager);

            recyclerView_Adapter = new Recyclerview(context,numbers);

            recyclerView.setAdapter(recyclerView_Adapter);*/
           List<String> cate=new ArrayList<String>();

            defectitem = new ArrayList<Defectmodel>();
            ListView lview = (ListView) v.findViewById(R.id.recyclerview1);
            DefectAdapter adapter = new DefectAdapter(this, defectitem);
            lview.setAdapter(adapter);

            populateList();

            adapter.notifyDataSetChanged();

            lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

            String duedate = ((TextView)view.findViewById(R.id.textview)).getText().toString();
                    String code = ((TextView)view.findViewById(R.id.Dtv1)).getText().toString();
          String defecttext = ((TextView)view.findViewById(R.id.Dtv2)).getText().toString();
                    String correctionaction = ((TextView)view.findViewById(R.id.Dtv3)).getText().toString();
                    Toast.makeText(getActivity().getApplicationContext(),
                          duedate +"\n"
                                    + code +"\n"
                                    +defecttext +"\n"
                                    +correctionaction , Toast.LENGTH_SHORT).show();
                }
            });
           return v;
       }
    private void populateList() {

        Defectmodel item1, item2, item3, item4, item5,item6,item7;

        item1 = new Defectmodel("27/04/2015", "061>>06107", "TES-E-05/2015,DEFECTIVE A/E FLOW METERS IN & OUT , NO MOVEMENT FLOW", "SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E Spare mass flowmeters received, to be installed in the next DD,presently old gears of the FM overhauled and reused");
        defectitem.add(item1);

        item2 = new Defectmodel("27/04/2015", "061>>06107", "TES-E-05/2015,DEFECTIVE A/E FLOW METERS IN & OUT , NO MOVEMENT FLOW", "SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E Spare mass flowmeters received, to be installed in the next DD,presently old gears of the FM overhauled and reused");
        defectitem.add(item2);

        item3 = new Defectmodel("27/04/2015", "061>>06107", "TES-E-05/2015,DEFECTIVE A/E FLOW METERS IN & OUT , NO MOVEMENT FLOW", "SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E Spare mass flowmeters received, to be installed in the next DD,presently old gears of the FM overhauled and reused");
        defectitem.add(item3);

        item4 = new Defectmodel("27/04/2015", "061>>06107", "TES-E-05/2015,DEFECTIVE A/E FLOW METERS IN & OUT , NO MOVEMENT FLOW", "SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E Spare mass flowmeters received, to be installed in the next DD,presently old gears of the FM overhauled and reused");
        defectitem.add(item4);

        item5 = new Defectmodel("27/04/2015", "061>>06107", "TES-E-05/2015,DEFECTIVE A/E FLOW METERS IN & OUT , NO MOVEMENT FLOW", "SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E Spare mass flowmeters received, to be installed in the next DD,presently old gears of the FM overhauled and reused");
        defectitem.add(item5);
        item6 = new Defectmodel("27/04/2015", "061>>06107", "TES-E-05/2015,DEFECTIVE A/E FLOW METERS IN & OUT , NO MOVEMENT FLOW", "SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E Spare mass flowmeters received, to be installed in the next DD,presently old gears of the FM overhauled and reused");
        defectitem.add(item6);
        item7 = new Defectmodel("27/04/2015", "061>>06107", "TES-E-05/2015,DEFECTIVE A/E FLOW METERS IN & OUT , NO MOVEMENT FLOW", "SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E Spare mass flowmeters received, to be installed in the next DD,presently old gears of the FM overhauled and reused");
        defectitem.add(item7);
    }


}

