
package com.example.modifyummsapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserMaster {

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("DepartmentId")
    @Expose
    private String departmentId;
    @SerializedName("RoleId")
    @Expose
    private String roleId;
    @SerializedName("VesselType")
    @Expose
    private String vesselType;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("UserSetting")
    @Expose
    private Object userSetting;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getVesselType() {
        return vesselType;
    }

    public void setVesselType(String vesselType) {
        this.vesselType = vesselType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getUserSetting() {
        return userSetting;
    }

    public void setUserSetting(Object userSetting) {
        this.userSetting = userSetting;
    }

}
