
package com.example.modifyummsapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfficeUser {

    @SerializedName("OfficeUserId")
    @Expose
    private String officeUserId;
    @SerializedName("SeacastEMPCode")
    @Expose
    private String seacastEMPCode;
    @SerializedName("Prefix")
    @Expose
    private String prefix;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private String middleName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("ProfileImage")
    @Expose
    private Object profileImage;

    public String getOfficeUserId() {
        return officeUserId;
    }

    public void setOfficeUserId(String officeUserId) {
        this.officeUserId = officeUserId;
    }

    public String getSeacastEMPCode() {
        return seacastEMPCode;
    }

    public void setSeacastEMPCode(String seacastEMPCode) {
        this.seacastEMPCode = seacastEMPCode;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Object getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Object profileImage) {
        this.profileImage = profileImage;
    }

}
