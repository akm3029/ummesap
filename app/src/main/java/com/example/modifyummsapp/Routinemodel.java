package com.example.modifyummsapp;

public class Routinemodel {
    private String jobnumber;
    private String classjob;
    private String jobname;
    private String scheduleduedate;
    private String rank;
    private String plandate;


    public Routinemodel(String jobnumber, String classjob, String jobname,  String scheduleduedate,String rank,String plandate) {
        this.jobnumber = jobnumber;
        this.classjob = classjob;
        this.jobname = jobname;
        this.scheduleduedate=scheduleduedate;
        this.rank=rank;
        this.plandate=plandate;
    }

    public String getJobnumber() {
        return jobnumber;
    }

    public String getClassjob() {
        return classjob;
    }

    public String getJobname() {
        return jobname;
    }

    public String getScheduleduedate() {
        return scheduleduedate;
    }
    public String getRank() {
        return rank;
    }
    public String getPlandate() {
        return plandate;
    }

}
