package com.example.modifyummsapp.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse implements Serializable {

    /**
     * status : success
     * message :
     * data : {"UserMaster":{"Token":"080c993fb3b58e26c1d2265bf9da0af3","DepartmentId":"1009","RoleId":"12","VesselType":"All","Title":null,"UserSetting":null},"OfficeUser":{"OfficeUserId":"142","SeacastEMPCode":null,"Prefix":"Mr.","FirstName":"Allan","MiddleName":"","LastName":"Ausan","Gender":"Male","ProfileImage":null},"CrewInfo":{"ApplicantId":null,"CompanyEMPCode":null,"FirstName":null,"MiddleName":null,"LastName":null,"SignedOnDate":null,"SignedOffDate":null,"ContractExpiryDate":null},"VesselList":{"74":"AFRICAN FLAMINGO","11":"ALIANCA ORIENT","49":"ALTAIR SKY","44":"AP ASTAREA","32":"AP DRZIC","45":"AP SLANO","46":"AP STON","36":"AP SVETI VLAHO","63":"ATLANTIC GRACE","10":"BAROCK","75":"BUNJI","12":"CASTA DIVA","22":"CASTOR STAR","38":"CATHERINE MANX","40":"CF DIAMOND","29":"CONON","56":"CRYSTAL ARROW","76":"FD ANGELICA","24":"FD ANGELICA (OLD)","1":"FD ISABELLA","47":"FORTUNE SYMPHONY","58":"FUJISUKA","62":"FUKUYAMA STAR","39":"GERALDINE MANX","2":"HOEGH BRASILIA","13":"HOEGH SYDNEY","3":"IDM SYMEX","54":"IOANNA L","79":"IRIS HARMONY","14":"JO JIN MARU","42":"KAYA MANX","77":"KK PIRAPO","15":"KT CONDOR","33":"LACON","73":"LAURENCE FRANCOISE","34":"LENA B","67":"LIVIA ROSE","51":"LOWLANDS BEACON","23":"MAJULAH SINGAPURA","37":"MIKA MANX","48":"MONA MANX","30":"OCEAN ADVENTURE","43":"OLUJA","57":"ORIENTAL ARROW","21":"POLLUX STAR","66":"PROJECT - GOLDEN CROWN","71":"PROJECT - SS264","68":"PURE TRADER","69":"PURE VISION","41":"RIKKE","78":"ROSE HARMONY","4":"SAFE VOYAGER","55":"SARAH","64":"SCARLETT MANX","50":"SVETI NIKOLA I","27":"TEN YOSHI MARU","19":"TEN YOSHI MARU (OLD)","7":"TEN YU MARU","5":"TENKI MARU","8":"TENMYO MARU","28":"TENRO MARU","9":"TENSEI MARU","20":"TENSHIN MARU","16":"TENSHOU MARU","61":"TENWA MARU","86":"TEST","87":"TEST VESSEL","88":"TEST VESSEL","17":"TRIPLE EVER","18":"TRIPLE STAR","26":"ULTRA COUGAR","31":"ULTRA JAGUAR","25":"ULTRA LION","72":"ULTRA PROGRESSION","-1":"UMMS OFFICE","53":"VEGA SKY"}}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * UserMaster : {"Token":"080c993fb3b58e26c1d2265bf9da0af3","DepartmentId":"1009","RoleId":"12","VesselType":"All","Title":null,"UserSetting":null}
         * OfficeUser : {"OfficeUserId":"142","SeacastEMPCode":null,"Prefix":"Mr.","FirstName":"Allan","MiddleName":"","LastName":"Ausan","Gender":"Male","ProfileImage":null}
         * CrewInfo : {"ApplicantId":null,"CompanyEMPCode":null,"FirstName":null,"MiddleName":null,"LastName":null,"SignedOnDate":null,"SignedOffDate":null,"ContractExpiryDate":null}
         * VesselList : {"74":"AFRICAN FLAMINGO","11":"ALIANCA ORIENT","49":"ALTAIR SKY","44":"AP ASTAREA","32":"AP DRZIC","45":"AP SLANO","46":"AP STON","36":"AP SVETI VLAHO","63":"ATLANTIC GRACE","10":"BAROCK","75":"BUNJI","12":"CASTA DIVA","22":"CASTOR STAR","38":"CATHERINE MANX","40":"CF DIAMOND","29":"CONON","56":"CRYSTAL ARROW","76":"FD ANGELICA","24":"FD ANGELICA (OLD)","1":"FD ISABELLA","47":"FORTUNE SYMPHONY","58":"FUJISUKA","62":"FUKUYAMA STAR","39":"GERALDINE MANX","2":"HOEGH BRASILIA","13":"HOEGH SYDNEY","3":"IDM SYMEX","54":"IOANNA L","79":"IRIS HARMONY","14":"JO JIN MARU","42":"KAYA MANX","77":"KK PIRAPO","15":"KT CONDOR","33":"LACON","73":"LAURENCE FRANCOISE","34":"LENA B","67":"LIVIA ROSE","51":"LOWLANDS BEACON","23":"MAJULAH SINGAPURA","37":"MIKA MANX","48":"MONA MANX","30":"OCEAN ADVENTURE","43":"OLUJA","57":"ORIENTAL ARROW","21":"POLLUX STAR","66":"PROJECT - GOLDEN CROWN","71":"PROJECT - SS264","68":"PURE TRADER","69":"PURE VISION","41":"RIKKE","78":"ROSE HARMONY","4":"SAFE VOYAGER","55":"SARAH","64":"SCARLETT MANX","50":"SVETI NIKOLA I","27":"TEN YOSHI MARU","19":"TEN YOSHI MARU (OLD)","7":"TEN YU MARU","5":"TENKI MARU","8":"TENMYO MARU","28":"TENRO MARU","9":"TENSEI MARU","20":"TENSHIN MARU","16":"TENSHOU MARU","61":"TENWA MARU","86":"TEST","87":"TEST VESSEL","88":"TEST VESSEL","17":"TRIPLE EVER","18":"TRIPLE STAR","26":"ULTRA COUGAR","31":"ULTRA JAGUAR","25":"ULTRA LION","72":"ULTRA PROGRESSION","-1":"UMMS OFFICE","53":"VEGA SKY"}
         */

        private UserMasterBean UserMaster;
        private OfficeUserBean OfficeUser;
        private CrewInfoBean CrewInfo;
        private VesselListBean VesselList;

        public UserMasterBean getUserMaster() {
            return UserMaster;
        }

        public void setUserMaster(UserMasterBean UserMaster) {
            this.UserMaster = UserMaster;
        }

        public OfficeUserBean getOfficeUser() {
            return OfficeUser;
        }

        public void setOfficeUser(OfficeUserBean OfficeUser) {
            this.OfficeUser = OfficeUser;
        }

        public CrewInfoBean getCrewInfo() {
            return CrewInfo;
        }

        public void setCrewInfo(CrewInfoBean CrewInfo) {
            this.CrewInfo = CrewInfo;
        }

        public VesselListBean getVesselList() {
            return VesselList;
        }

        public void setVesselList(VesselListBean VesselList) {
            this.VesselList = VesselList;
        }

        public static class UserMasterBean {
            /**
             * Token : 080c993fb3b58e26c1d2265bf9da0af3
             * DepartmentId : 1009
             * RoleId : 12
             * VesselType : All
             * Title : null
             * UserSetting : null
             */

            private String Token;
            private String DepartmentId;
            private String RoleId;
            private String VesselType;
            private Object Title;
            private Object UserSetting;

            public String getToken() {
                return Token;
            }

            public void setToken(String Token) {
                this.Token = Token;
            }

            public String getDepartmentId() {
                return DepartmentId;
            }

            public void setDepartmentId(String DepartmentId) {
                this.DepartmentId = DepartmentId;
            }

            public String getRoleId() {
                return RoleId;
            }

            public void setRoleId(String RoleId) {
                this.RoleId = RoleId;
            }

            public String getVesselType() {
                return VesselType;
            }

            public void setVesselType(String VesselType) {
                this.VesselType = VesselType;
            }

            public Object getTitle() {
                return Title;
            }

            public void setTitle(Object Title) {
                this.Title = Title;
            }

            public Object getUserSetting() {
                return UserSetting;
            }

            public void setUserSetting(Object UserSetting) {
                this.UserSetting = UserSetting;
            }
        }

        public static class OfficeUserBean {
            /**
             * OfficeUserId : 142
             * SeacastEMPCode : null
             * Prefix : Mr.
             * FirstName : Allan
             * MiddleName :
             * LastName : Ausan
             * Gender : Male
             * ProfileImage : null
             */

            private String OfficeUserId;
            private Object SeacastEMPCode;
            private String Prefix;
            private String FirstName;
            private String MiddleName;
            private String LastName;
            private String Gender;
            private Object ProfileImage;

            public String getOfficeUserId() {
                return OfficeUserId;
            }

            public void setOfficeUserId(String OfficeUserId) {
                this.OfficeUserId = OfficeUserId;
            }

            public Object getSeacastEMPCode() {
                return SeacastEMPCode;
            }

            public void setSeacastEMPCode(Object SeacastEMPCode) {
                this.SeacastEMPCode = SeacastEMPCode;
            }

            public String getPrefix() {
                return Prefix;
            }

            public void setPrefix(String Prefix) {
                this.Prefix = Prefix;
            }

            public String getFirstName() {
                return FirstName;
            }

            public void setFirstName(String FirstName) {
                this.FirstName = FirstName;
            }

            public String getMiddleName() {
                return MiddleName;
            }

            public void setMiddleName(String MiddleName) {
                this.MiddleName = MiddleName;
            }

            public String getLastName() {
                return LastName;
            }

            public void setLastName(String LastName) {
                this.LastName = LastName;
            }

            public String getGender() {
                return Gender;
            }

            public void setGender(String Gender) {
                this.Gender = Gender;
            }

            public Object getProfileImage() {
                return ProfileImage;
            }

            public void setProfileImage(Object ProfileImage) {
                this.ProfileImage = ProfileImage;
            }
        }

        public static class CrewInfoBean {
            /**
             * ApplicantId : null
             * CompanyEMPCode : null
             * FirstName : null
             * MiddleName : null
             * LastName : null
             * SignedOnDate : null
             * SignedOffDate : null
             * ContractExpiryDate : null
             */

            private Object ApplicantId;
            private Object CompanyEMPCode;
            private Object FirstName;
            private Object MiddleName;
            private Object LastName;
            private Object SignedOnDate;
            private Object SignedOffDate;
            private Object ContractExpiryDate;

            public Object getApplicantId() {
                return ApplicantId;
            }

            public void setApplicantId(Object ApplicantId) {
                this.ApplicantId = ApplicantId;
            }

            public Object getCompanyEMPCode() {
                return CompanyEMPCode;
            }

            public void setCompanyEMPCode(Object CompanyEMPCode) {
                this.CompanyEMPCode = CompanyEMPCode;
            }

            public Object getFirstName() {
                return FirstName;
            }

            public void setFirstName(Object FirstName) {
                this.FirstName = FirstName;
            }

            public Object getMiddleName() {
                return MiddleName;
            }

            public void setMiddleName(Object MiddleName) {
                this.MiddleName = MiddleName;
            }

            public Object getLastName() {
                return LastName;
            }

            public void setLastName(Object LastName) {
                this.LastName = LastName;
            }

            public Object getSignedOnDate() {
                return SignedOnDate;
            }

            public void setSignedOnDate(Object SignedOnDate) {
                this.SignedOnDate = SignedOnDate;
            }

            public Object getSignedOffDate() {
                return SignedOffDate;
            }

            public void setSignedOffDate(Object SignedOffDate) {
                this.SignedOffDate = SignedOffDate;
            }

            public Object getContractExpiryDate() {
                return ContractExpiryDate;
            }

            public void setContractExpiryDate(Object ContractExpiryDate) {
                this.ContractExpiryDate = ContractExpiryDate;
            }
        }

        public static class VesselListBean {
            /**
             * 74 : AFRICAN FLAMINGO
             * 11 : ALIANCA ORIENT
             * 49 : ALTAIR SKY
             * 44 : AP ASTAREA
             * 32 : AP DRZIC
             * 45 : AP SLANO
             * 46 : AP STON
             * 36 : AP SVETI VLAHO
             * 63 : ATLANTIC GRACE
             * 10 : BAROCK
             * 75 : BUNJI
             * 12 : CASTA DIVA
             * 22 : CASTOR STAR
             * 38 : CATHERINE MANX
             * 40 : CF DIAMOND
             * 29 : CONON
             * 56 : CRYSTAL ARROW
             * 76 : FD ANGELICA
             * 24 : FD ANGELICA (OLD)
             * 1 : FD ISABELLA
             * 47 : FORTUNE SYMPHONY
             * 58 : FUJISUKA
             * 62 : FUKUYAMA STAR
             * 39 : GERALDINE MANX
             * 2 : HOEGH BRASILIA
             * 13 : HOEGH SYDNEY
             * 3 : IDM SYMEX
             * 54 : IOANNA L
             * 79 : IRIS HARMONY
             * 14 : JO JIN MARU
             * 42 : KAYA MANX
             * 77 : KK PIRAPO
             * 15 : KT CONDOR
             * 33 : LACON
             * 73 : LAURENCE FRANCOISE
             * 34 : LENA B
             * 67 : LIVIA ROSE
             * 51 : LOWLANDS BEACON
             * 23 : MAJULAH SINGAPURA
             * 37 : MIKA MANX
             * 48 : MONA MANX
             * 30 : OCEAN ADVENTURE
             * 43 : OLUJA
             * 57 : ORIENTAL ARROW
             * 21 : POLLUX STAR
             * 66 : PROJECT - GOLDEN CROWN
             * 71 : PROJECT - SS264
             * 68 : PURE TRADER
             * 69 : PURE VISION
             * 41 : RIKKE
             * 78 : ROSE HARMONY
             * 4 : SAFE VOYAGER
             * 55 : SARAH
             * 64 : SCARLETT MANX
             * 50 : SVETI NIKOLA I
             * 27 : TEN YOSHI MARU
             * 19 : TEN YOSHI MARU (OLD)
             * 7 : TEN YU MARU
             * 5 : TENKI MARU
             * 8 : TENMYO MARU
             * 28 : TENRO MARU
             * 9 : TENSEI MARU
             * 20 : TENSHIN MARU
             * 16 : TENSHOU MARU
             * 61 : TENWA MARU
             * 86 : TEST
             * 87 : TEST VESSEL
             * 88 : TEST VESSEL
             * 17 : TRIPLE EVER
             * 18 : TRIPLE STAR
             * 26 : ULTRA COUGAR
             * 31 : ULTRA JAGUAR
             * 25 : ULTRA LION
             * 72 : ULTRA PROGRESSION
             * -1 : UMMS OFFICE
             * 53 : VEGA SKY
             */

            @SerializedName("74")
            private String _$74;
            @SerializedName("11")
            private String _$11;
            @SerializedName("49")
            private String _$49;
            @SerializedName("44")
            private String _$44;
            @SerializedName("32")
            private String _$32;
            @SerializedName("45")
            private String _$45;
            @SerializedName("46")
            private String _$46;
            @SerializedName("36")
            private String _$36;
            @SerializedName("63")
            private String _$63;
            @SerializedName("10")
            private String _$10;
            @SerializedName("75")
            private String _$75;
            @SerializedName("12")
            private String _$12;
            @SerializedName("22")
            private String _$22;
            @SerializedName("38")
            private String _$38;
            @SerializedName("40")
            private String _$40;
            @SerializedName("29")
            private String _$29;
            @SerializedName("56")
            private String _$56;
            @SerializedName("76")
            private String _$76;
            @SerializedName("24")
            private String _$24;
            @SerializedName("1")
            private String _$1;
            @SerializedName("47")
            private String _$47;
            @SerializedName("58")
            private String _$58;
            @SerializedName("62")
            private String _$62;
            @SerializedName("39")
            private String _$39;
            @SerializedName("2")
            private String _$2;
            @SerializedName("13")
            private String _$13;
            @SerializedName("3")
            private String _$3;
            @SerializedName("54")
            private String _$54;
            @SerializedName("79")
            private String _$79;
            @SerializedName("14")
            private String _$14;
            @SerializedName("42")
            private String _$42;
            @SerializedName("77")
            private String _$77;
            @SerializedName("15")
            private String _$15;
            @SerializedName("33")
            private String _$33;
            @SerializedName("73")
            private String _$73;
            @SerializedName("34")
            private String _$34;
            @SerializedName("67")
            private String _$67;
            @SerializedName("51")
            private String _$51;
            @SerializedName("23")
            private String _$23;
            @SerializedName("37")
            private String _$37;
            @SerializedName("48")
            private String _$48;
            @SerializedName("30")
            private String _$30;
            @SerializedName("43")
            private String _$43;
            @SerializedName("57")
            private String _$57;
            @SerializedName("21")
            private String _$21;
            @SerializedName("66")
            private String _$66;
            @SerializedName("71")
            private String _$71;
            @SerializedName("68")
            private String _$68;
            @SerializedName("69")
            private String _$69;
            @SerializedName("41")
            private String _$41;
            @SerializedName("78")
            private String _$78;
            @SerializedName("4")
            private String _$4;
            @SerializedName("55")
            private String _$55;
            @SerializedName("64")
            private String _$64;
            @SerializedName("50")
            private String _$50;
            @SerializedName("27")
            private String _$27;
            @SerializedName("19")
            private String _$19;
            @SerializedName("7")
            private String _$7;
            @SerializedName("5")
            private String _$5;
            @SerializedName("8")
            private String _$8;
            @SerializedName("28")
            private String _$28;
            @SerializedName("9")
            private String _$9;
            @SerializedName("20")
            private String _$20;
            @SerializedName("16")
            private String _$16;
            @SerializedName("61")
            private String _$61;
            @SerializedName("86")
            private String _$86;
            @SerializedName("87")
            private String _$87;
            @SerializedName("88")
            private String _$88;
            @SerializedName("17")
            private String _$17;
            @SerializedName("18")
            private String _$18;
            @SerializedName("26")
            private String _$26;
            @SerializedName("31")
            private String _$31;
            @SerializedName("25")
            private String _$25;
            @SerializedName("72")
            private String _$72;
            @SerializedName("-1")
            private String _$01;
            @SerializedName("53")
            private String _$53;

            public String get_$74() {
                return _$74;
            }

            public void set_$74(String _$74) {
                this._$74 = _$74;
            }

            public String get_$11() {
                return _$11;
            }

            public void set_$11(String _$11) {
                this._$11 = _$11;
            }

            public String get_$49() {
                return _$49;
            }

            public void set_$49(String _$49) {
                this._$49 = _$49;
            }

            public String get_$44() {
                return _$44;
            }

            public void set_$44(String _$44) {
                this._$44 = _$44;
            }

            public String get_$32() {
                return _$32;
            }

            public void set_$32(String _$32) {
                this._$32 = _$32;
            }

            public String get_$45() {
                return _$45;
            }

            public void set_$45(String _$45) {
                this._$45 = _$45;
            }

            public String get_$46() {
                return _$46;
            }

            public void set_$46(String _$46) {
                this._$46 = _$46;
            }

            public String get_$36() {
                return _$36;
            }

            public void set_$36(String _$36) {
                this._$36 = _$36;
            }

            public String get_$63() {
                return _$63;
            }

            public void set_$63(String _$63) {
                this._$63 = _$63;
            }

            public String get_$10() {
                return _$10;
            }

            public void set_$10(String _$10) {
                this._$10 = _$10;
            }

            public String get_$75() {
                return _$75;
            }

            public void set_$75(String _$75) {
                this._$75 = _$75;
            }

            public String get_$12() {
                return _$12;
            }

            public void set_$12(String _$12) {
                this._$12 = _$12;
            }

            public String get_$22() {
                return _$22;
            }

            public void set_$22(String _$22) {
                this._$22 = _$22;
            }

            public String get_$38() {
                return _$38;
            }

            public void set_$38(String _$38) {
                this._$38 = _$38;
            }

            public String get_$40() {
                return _$40;
            }

            public void set_$40(String _$40) {
                this._$40 = _$40;
            }

            public String get_$29() {
                return _$29;
            }

            public void set_$29(String _$29) {
                this._$29 = _$29;
            }

            public String get_$56() {
                return _$56;
            }

            public void set_$56(String _$56) {
                this._$56 = _$56;
            }

            public String get_$76() {
                return _$76;
            }

            public void set_$76(String _$76) {
                this._$76 = _$76;
            }

            public String get_$24() {
                return _$24;
            }

            public void set_$24(String _$24) {
                this._$24 = _$24;
            }

            public String get_$1() {
                return _$1;
            }

            public void set_0$1(String _$01) {
                this._$1 = _$1;
            }

            public String get_$47() {
                return _$47;
            }

            public void set_$47(String _$47) {
                this._$47 = _$47;
            }

            public String get_$58() {
                return _$58;
            }

            public void set_$58(String _$58) {
                this._$58 = _$58;
            }

            public String get_$62() {
                return _$62;
            }

            public void set_$62(String _$62) {
                this._$62 = _$62;
            }

            public String get_$39() {
                return _$39;
            }

            public void set_$39(String _$39) {
                this._$39 = _$39;
            }

            public String get_$2() {
                return _$2;
            }

            public void set_$2(String _$2) {
                this._$2 = _$2;
            }

            public String get_$13() {
                return _$13;
            }

            public void set_$13(String _$13) {
                this._$13 = _$13;
            }

            public String get_$3() {
                return _$3;
            }

            public void set_$3(String _$3) {
                this._$3 = _$3;
            }

            public String get_$54() {
                return _$54;
            }

            public void set_$54(String _$54) {
                this._$54 = _$54;
            }

            public String get_$79() {
                return _$79;
            }

            public void set_$79(String _$79) {
                this._$79 = _$79;
            }

            public String get_$14() {
                return _$14;
            }

            public void set_$14(String _$14) {
                this._$14 = _$14;
            }

            public String get_$42() {
                return _$42;
            }

            public void set_$42(String _$42) {
                this._$42 = _$42;
            }

            public String get_$77() {
                return _$77;
            }

            public void set_$77(String _$77) {
                this._$77 = _$77;
            }

            public String get_$15() {
                return _$15;
            }

            public void set_$15(String _$15) {
                this._$15 = _$15;
            }

            public String get_$33() {
                return _$33;
            }

            public void set_$33(String _$33) {
                this._$33 = _$33;
            }

            public String get_$73() {
                return _$73;
            }

            public void set_$73(String _$73) {
                this._$73 = _$73;
            }

            public String get_$34() {
                return _$34;
            }

            public void set_$34(String _$34) {
                this._$34 = _$34;
            }

            public String get_$67() {
                return _$67;
            }

            public void set_$67(String _$67) {
                this._$67 = _$67;
            }

            public String get_$51() {
                return _$51;
            }

            public void set_$51(String _$51) {
                this._$51 = _$51;
            }

            public String get_$23() {
                return _$23;
            }

            public void set_$23(String _$23) {
                this._$23 = _$23;
            }

            public String get_$37() {
                return _$37;
            }

            public void set_$37(String _$37) {
                this._$37 = _$37;
            }

            public String get_$48() {
                return _$48;
            }

            public void set_$48(String _$48) {
                this._$48 = _$48;
            }

            public String get_$30() {
                return _$30;
            }

            public void set_$30(String _$30) {
                this._$30 = _$30;
            }

            public String get_$43() {
                return _$43;
            }

            public void set_$43(String _$43) {
                this._$43 = _$43;
            }

            public String get_$57() {
                return _$57;
            }

            public void set_$57(String _$57) {
                this._$57 = _$57;
            }

            public String get_$21() {
                return _$21;
            }

            public void set_$21(String _$21) {
                this._$21 = _$21;
            }

            public String get_$66() {
                return _$66;
            }

            public void set_$66(String _$66) {
                this._$66 = _$66;
            }

            public String get_$71() {
                return _$71;
            }

            public void set_$71(String _$71) {
                this._$71 = _$71;
            }

            public String get_$68() {
                return _$68;
            }

            public void set_$68(String _$68) {
                this._$68 = _$68;
            }

            public String get_$69() {
                return _$69;
            }

            public void set_$69(String _$69) {
                this._$69 = _$69;
            }

            public String get_$41() {
                return _$41;
            }

            public void set_$41(String _$41) {
                this._$41 = _$41;
            }

            public String get_$78() {
                return _$78;
            }

            public void set_$78(String _$78) {
                this._$78 = _$78;
            }

            public String get_$4() {
                return _$4;
            }

            public void set_$4(String _$4) {
                this._$4 = _$4;
            }

            public String get_$55() {
                return _$55;
            }

            public void set_$55(String _$55) {
                this._$55 = _$55;
            }

            public String get_$64() {
                return _$64;
            }

            public void set_$64(String _$64) {
                this._$64 = _$64;
            }

            public String get_$50() {
                return _$50;
            }

            public void set_$50(String _$50) {
                this._$50 = _$50;
            }

            public String get_$27() {
                return _$27;
            }

            public void set_$27(String _$27) {
                this._$27 = _$27;
            }

            public String get_$19() {
                return _$19;
            }

            public void set_$19(String _$19) {
                this._$19 = _$19;
            }

            public String get_$7() {
                return _$7;
            }

            public void set_$7(String _$7) {
                this._$7 = _$7;
            }

            public String get_$5() {
                return _$5;
            }

            public void set_$5(String _$5) {
                this._$5 = _$5;
            }

            public String get_$8() {
                return _$8;
            }

            public void set_$8(String _$8) {
                this._$8 = _$8;
            }

            public String get_$28() {
                return _$28;
            }

            public void set_$28(String _$28) {
                this._$28 = _$28;
            }

            public String get_$9() {
                return _$9;
            }

            public void set_$9(String _$9) {
                this._$9 = _$9;
            }

            public String get_$20() {
                return _$20;
            }

            public void set_$20(String _$20) {
                this._$20 = _$20;
            }

            public String get_$16() {
                return _$16;
            }

            public void set_$16(String _$16) {
                this._$16 = _$16;
            }

            public String get_$61() {
                return _$61;
            }

            public void set_$61(String _$61) {
                this._$61 = _$61;
            }

            public String get_$86() {
                return _$86;
            }

            public void set_$86(String _$86) {
                this._$86 = _$86;
            }

            public String get_$87() {
                return _$87;
            }

            public void set_$87(String _$87) {
                this._$87 = _$87;
            }

            public String get_$88() {
                return _$88;
            }

            public void set_$88(String _$88) {
                this._$88 = _$88;
            }

            public String get_$17() {
                return _$17;
            }

            public void set_$17(String _$17) {
                this._$17 = _$17;
            }

            public String get_$18() {
                return _$18;
            }

            public void set_$18(String _$18) {
                this._$18 = _$18;
            }

            public String get_$26() {
                return _$26;
            }

            public void set_$26(String _$26) {
                this._$26 = _$26;
            }

            public String get_$31() {
                return _$31;
            }

            public void set_$31(String _$31) {
                this._$31 = _$31;
            }

            public String get_$25() {
                return _$25;
            }

            public void set_$25(String _$25) {
                this._$25 = _$25;
            }

            public String get_$72() {
                return _$72;
            }

            public void set_$72(String _$72) {
                this._$72 = _$72;
            }

            public String get_$01() {
                return _$01;
            }

            public void set_$01(String _$01) {
                this._$1 = _$01;
            }

            public String get_$53() {
                return _$53;
            }

            public void set_$53(String _$53) {
                this._$53 = _$53;
            }
        }
    }
}
