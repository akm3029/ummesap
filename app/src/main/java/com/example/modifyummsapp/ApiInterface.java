package com.example.modifyummsapp;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    String JSONURL = "https://www.testintuitships-umms.com/";

    @GET("mobile_api/workplan/get")
    Call<String> getString();
}


