
package com.example.modifyummsapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VesselList {

    @SerializedName("74")
    @Expose
    private String _74;
    @SerializedName("11")
    @Expose
    private String _11;
    @SerializedName("44")
    @Expose
    private String _44;
    @SerializedName("45")
    @Expose
    private String _45;
    @SerializedName("46")
    @Expose
    private String _46;
    @SerializedName("12")
    @Expose
    private String _12;
    @SerializedName("40")
    @Expose
    private String _40;
    @SerializedName("56")
    @Expose
    private String _56;
    @SerializedName("47")
    @Expose
    private String _47;
    @SerializedName("58")
    @Expose
    private String _58;
    @SerializedName("3")
    @Expose
    private String _3;
    @SerializedName("30")
    @Expose
    private String _30;
    @SerializedName("57")
    @Expose
    private String _57;
    @SerializedName("21")
    @Expose
    private String _21;
    @SerializedName("55")
    @Expose
    private String _55;
    @SerializedName("28")
    @Expose
    private String _28;
    @SerializedName("9")
    @Expose
    private String _9;
    @SerializedName("16")
    @Expose
    private String _16;
    @SerializedName("86")
    @Expose
    private String _86;
    @SerializedName("87")
    @Expose
    private String _87;
    @SerializedName("88")
    @Expose
    private String _88;
    @SerializedName("17")
    @Expose
    private String _17;
    @SerializedName("18")
    @Expose
    private String _18;
    @SerializedName("25")
    @Expose
    private String _25;

    public String get74() {
        return _74;
    }

    public void set74(String _74) {
        this._74 = _74;
    }

    public String get11() {
        return _11;
    }

    public void set11(String _11) {
        this._11 = _11;
    }

    public String get44() {
        return _44;
    }

    public void set44(String _44) {
        this._44 = _44;
    }

    public String get45() {
        return _45;
    }

    public void set45(String _45) {
        this._45 = _45;
    }

    public String get46() {
        return _46;
    }

    public void set46(String _46) {
        this._46 = _46;
    }

    public String get12() {
        return _12;
    }

    public void set12(String _12) {
        this._12 = _12;
    }

    public String get40() {
        return _40;
    }

    public void set40(String _40) {
        this._40 = _40;
    }

    public String get56() {
        return _56;
    }

    public void set56(String _56) {
        this._56 = _56;
    }

    public String get47() {
        return _47;
    }

    public void set47(String _47) {
        this._47 = _47;
    }

    public String get58() {
        return _58;
    }

    public void set58(String _58) {
        this._58 = _58;
    }

    public String get3() {
        return _3;
    }

    public void set3(String _3) {
        this._3 = _3;
    }

    public String get30() {
        return _30;
    }

    public void set30(String _30) {
        this._30 = _30;
    }

    public String get57() {
        return _57;
    }

    public void set57(String _57) {
        this._57 = _57;
    }

    public String get21() {
        return _21;
    }

    public void set21(String _21) {
        this._21 = _21;
    }

    public String get55() {
        return _55;
    }

    public void set55(String _55) {
        this._55 = _55;
    }

    public String get28() {
        return _28;
    }

    public void set28(String _28) {
        this._28 = _28;
    }

    public String get9() {
        return _9;
    }

    public void set9(String _9) {
        this._9 = _9;
    }

    public String get16() {
        return _16;
    }

    public void set16(String _16) {
        this._16 = _16;
    }

    public String get86() {
        return _86;
    }

    public void set86(String _86) {
        this._86 = _86;
    }

    public String get87() {
        return _87;
    }

    public void set87(String _87) {
        this._87 = _87;
    }

    public String get88() {
        return _88;
    }

    public void set88(String _88) {
        this._88 = _88;
    }

    public String get17() {
        return _17;
    }

    public void set17(String _17) {
        this._17 = _17;
    }

    public String get18() {
        return _18;
    }

    public void set18(String _18) {
        this._18 = _18;
    }

    public String get25() {
        return _25;
    }

    public void set25(String _25) {
        this._25 = _25;
    }

}
