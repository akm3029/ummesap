
package com.example.modifyummsapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CrewInfo {

    @SerializedName("ApplicantId")
    @Expose
    private Object applicantId;
    @SerializedName("CompanyEMPCode")
    @Expose
    private Object companyEMPCode;
    @SerializedName("FirstName")
    @Expose
    private Object firstName;
    @SerializedName("MiddleName")
    @Expose
    private Object middleName;
    @SerializedName("LastName")
    @Expose
    private Object lastName;
    @SerializedName("SignedOnDate")
    @Expose
    private Object signedOnDate;
    @SerializedName("SignedOffDate")
    @Expose
    private Object signedOffDate;
    @SerializedName("ContractExpiryDate")
    @Expose
    private Object contractExpiryDate;

    public Object getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Object applicantId) {
        this.applicantId = applicantId;
    }

    public Object getCompanyEMPCode() {
        return companyEMPCode;
    }

    public void setCompanyEMPCode(Object companyEMPCode) {
        this.companyEMPCode = companyEMPCode;
    }

    public Object getFirstName() {
        return firstName;
    }

    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    public Object getMiddleName() {
        return middleName;
    }

    public void setMiddleName(Object middleName) {
        this.middleName = middleName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public Object getSignedOnDate() {
        return signedOnDate;
    }

    public void setSignedOnDate(Object signedOnDate) {
        this.signedOnDate = signedOnDate;
    }

    public Object getSignedOffDate() {
        return signedOffDate;
    }

    public void setSignedOffDate(Object signedOffDate) {
        this.signedOffDate = signedOffDate;
    }

    public Object getContractExpiryDate() {
        return contractExpiryDate;
    }

    public void setContractExpiryDate(Object contractExpiryDate) {
        this.contractExpiryDate = contractExpiryDate;
    }

}
