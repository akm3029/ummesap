package com.example.modifyummsapp;

public class Defectmodel {

    private String duedate;
    private String code;
    private String defecttext;
    private String correctionaction;


    public Defectmodel(String duedate, String code, String defecttext,  String correctionaction) {
        this.duedate = duedate;
       this.code = code;
       this.defecttext = defecttext;
       this.correctionaction=correctionaction;
    }

    public String getDuedate() {
        return duedate;
    }

    public String getCode() {
        return code;
    }

    public String getDefecttext() {
        return defecttext;
    }

    public String getCorrectionaction() {
        return correctionaction;
    }

    //public void setDuedate(String duedate) {
     //   this.duedate = duedate;
    //}

   // public void setCode(String code) {
       // this.code = code;
   // }

   // public void setDefecttext(String defecttext) {
    //    this.defecttext = defecttext;
   // }

   // public void setCorrectionaction(String correctionaction) {
   //     this.correctionaction = correctionaction;
//    }
}
