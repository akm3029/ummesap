package com.example.modifyummsapp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RoutineFragment extends Fragment {
  View v;
    private ArrayList< Routinemodel> routineitem;
  // Context context;
   //RecyclerView.Adapter recyclerViewAdapter;
    //RecyclerView recyclerView;
    //RecyclerView.LayoutManager layoutManager;
    /*String[] depart={
            "one",
            "one",
            "one",
            "one",
            "one",
            "one",
            "one",
            "one",
            "one",
            "one",
            "one",
            "one",
            "one",
    };*/
    public RoutineFragment() {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v= inflater.inflate(R.layout.fragment_routine, container, false);
        /*context= getActivity().getApplicationContext();
        recyclerView=(RecyclerView)v.findViewById(R.id.recyclerview2);
        layoutManager=new GridLayoutManager(context,1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerViewAdapter=new Recyclerview1(context,depart);
        recyclerView.setAdapter(recyclerViewAdapter);*/

        routineitem = new ArrayList< Routinemodel>();
        ListView lview = (ListView) v.findViewById(R.id.recyclerview2);
        RoutineAdapter adapter = new RoutineAdapter(this, routineitem);
        lview.setAdapter(adapter);

        populateList();

        adapter.notifyDataSetChanged();

        lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String jobnumber = ((TextView)view.findViewById(R.id.Rtv1)).getText().toString();
                String classjob = ((TextView)view.findViewById(R.id.Rtv2)).getText().toString();
                String jobname = ((TextView)view.findViewById(R.id.Rtv3)).getText().toString();
                String scheduleduedate = ((TextView)view.findViewById(R.id.Rtv4)).getText().toString();
                String rank = ((TextView)view.findViewById(R.id.Rtv5)).getText().toString();
                String plandate = ((TextView)view.findViewById(R.id.Rtv6)).getText().toString();
                Toast.makeText(getActivity().getApplicationContext(),
                        jobnumber +"\n"
                                + classjob +"\n"
                                +jobname +"\n"
                                +scheduleduedate+"\n"
                                +rank+"\n"
                                +plandate, Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }
    private void populateList() {

        Routinemodel item1, item2, item3, item4, item5,item6,item7;

        item1 = new Routinemodel("861.000.RE", "No", "Unit Name:EMERGENCY GENERATOR Check: EMERGENCY GENERATOR -Replace V-Belt -Replace Dry Air Filter","13/11/2016","3/E","dd/mm/yyyy");
        routineitem.add(item1);

        item2 = new  Routinemodel("861.000.RE", "No", "Unit Name:EMERGENCY GENERATOR Check: EMERGENCY GENERATOR -Replace V-Belt -Replace Dry Air Filter", "13/11/2016","3/E","dd/mm/yyyy");
        routineitem.add(item2);

        item3 = new  Routinemodel("861.000.RE", "No", "Unit Name:EMERGENCY GENERATOR Check: EMERGENCY GENERATOR -Replace V-Belt -Replace Dry Air Filter", "13/11/2016","3/E","dd/mm/yyyy");
        routineitem.add(item3);

        item4 = new  Routinemodel("861.000.RE", "No", "Unit Name:EMERGENCY GENERATOR Check: EMERGENCY GENERATOR -Replace V-Belt -Replace Dry Air Filter", "13/11/2016","3/E","dd/mm/yyyy");
        routineitem.add(item4);

        item5 = new  Routinemodel("861.000.RE", "No", "Unit Name:EMERGENCY GENERATOR Check: EMERGENCY GENERATOR -Replace V-Belt -Replace Dry Air Filter", "13/11/2016","3/E","dd/mm/yyyy");
        routineitem.add(item5);
        item6 = new  Routinemodel("861.000.RE", "No", "Unit Name:EMERGENCY GENERATOR Check: EMERGENCY GENERATOR -Replace V-Belt -Replace Dry Air Filter", "13/11/2016","3/E","dd/mm/yyyy");
        routineitem.add(item6);
        item7 = new  Routinemodel("861.000.RE", "No", "Unit Name:EMERGENCY GENERATOR Check: EMERGENCY GENERATOR -Replace V-Belt -Replace Dry Air Filter", "13/11/2016","3/E","dd/mm/yyyy");
        routineitem.add(item7);
    }


}

