package com.example.modifyummsapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class RoutineAdapter extends BaseAdapter {
    private final RoutineFragment fragment;
    public ArrayList<Routinemodel> routineitem;

    public RoutineAdapter(RoutineFragment fragment, ArrayList<Routinemodel> routineitem) {
        super();
        this.fragment = fragment;
        this.routineitem= routineitem;
    }

    @Override
    public int getCount() {
        return routineitem.size();
    }

    @Override
    public Object getItem(int position) {
        return routineitem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView jobnumber;
        TextView classjob;
        TextView jobname;
        TextView scheduledduedate ;
        TextView rank ;
        TextView plandate ;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RoutineAdapter.ViewHolder holder;
        LayoutInflater inflater = fragment.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.recyclerview_routine, null);
            holder = new RoutineAdapter.ViewHolder();
            holder.jobnumber = (TextView) convertView.findViewById(R.id.Rtv1);
            holder.classjob = (TextView) convertView.findViewById(R.id.Rtv2);
            holder.jobname = (TextView) convertView.findViewById(R.id.Rtv3);
            holder.scheduledduedate = (TextView) convertView.findViewById(R.id.Rtv4);
            holder.rank = (TextView) convertView.findViewById(R.id.Rtv5);
            holder.plandate = (TextView) convertView.findViewById(R.id.Rtv6);
            convertView.setTag(holder);
        } else {
            holder = (RoutineAdapter.ViewHolder) convertView.getTag();
        }

        Routinemodel item = routineitem.get(position);
        holder.jobnumber.setText(item.getJobnumber().toString());
        holder.classjob.setText(item.getClassjob().toString());
        holder.jobname.setText(item.getJobname().toString());
        holder.scheduledduedate.setText(item.getScheduleduedate().toString());
        holder.rank.setText(item.getRank().toString());
        holder.plandate.setText(item.getPlandate().toString());


        return convertView;
    }
}
