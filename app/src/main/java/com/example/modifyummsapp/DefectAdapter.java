package com.example.modifyummsapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/*public class DefectAdapter extends RecyclerView.Adapter<DefectAdapter.MyViewHolder>{
    private LayoutInflater inflater;
    private ArrayList<Defectmodel> dataModelArrayList;

    public DefectAdapter(Context ctx, ArrayList<Defectmodel> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public DefectAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.recyclerview_defect, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(DefectAdapter.MyViewHolder holder, int position) {


        holder.duedate.setText(dataModelArrayList.get(position).getDuedate());
        holder.code.setText(dataModelArrayList.get(position).getCode());
        holder.defecttext.setText(dataModelArrayList.get(position).getDefecttext());
        holder.correctionaction.setText(dataModelArrayList.get(position).getCorrectionaction());
    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView duedate, code, defecttext,correctionaction;


        public MyViewHolder(View itemView) {
            super(itemView);

            duedate = (TextView) itemView.findViewById(R.id.textview);
            code = (TextView) itemView.findViewById(R.id.Dtv1);
            defecttext = (TextView) itemView.findViewById(R.id.Dtv2);
            correctionaction = (TextView) itemView.findViewById(R.id.Dtv3);
        }

    }
}*/

public class DefectAdapter extends BaseAdapter {
    private final DefectFragment fragment;
    public ArrayList<Defectmodel> defectitem;

    public DefectAdapter(DefectFragment fragment, ArrayList<Defectmodel> defectitem) {
        super();
        this.fragment = fragment;
        this.defectitem= defectitem;
    }

    @Override
    public int getCount() {
        return defectitem.size();
    }

    @Override
    public Object getItem(int position) {
        return defectitem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView duedate;
        TextView code;
        TextView defecttext;
        TextView correctionaction;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        LayoutInflater inflater = fragment.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.recyclerview_defect, null);
            holder = new ViewHolder();
            holder.duedate = (TextView) convertView.findViewById(R.id.textview);
            holder.code = (TextView) convertView.findViewById(R.id.Dtv1);
            holder.defecttext = (TextView) convertView.findViewById(R.id.Dtv2);
            holder.correctionaction = (TextView) convertView.findViewById(R.id.Dtv3);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Defectmodel item = defectitem.get(position);
        holder.duedate.setText(item.getDuedate().toString());
        holder.code.setText(item.getCode().toString());
        holder.defecttext.setText(item.getDefecttext().toString());
        holder.correctionaction.setText(item.getCorrectionaction().toString());

        return convertView;
    }
}
