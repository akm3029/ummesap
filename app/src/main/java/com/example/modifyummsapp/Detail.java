
package com.example.modifyummsapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("JobCode")
    @Expose
    private String jobCode;
    @SerializedName("JobType")
    @Expose
    private String jobType;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Class")
    @Expose
    private String _class;
    @SerializedName("Rank")
    @Expose
    private String rank;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

}
