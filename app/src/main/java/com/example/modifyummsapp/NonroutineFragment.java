package com.example.modifyummsapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class NonroutineFragment extends androidx.fragment.app.Fragment {
    Spinner sp1,sp2,sp3,sp4;
    View v;
    public NonroutineFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=inflater.inflate(R.layout.fragment_nonroutine, container, false);
        sp1=(Spinner)v.findViewById(R.id.sp1);
        sp2=(Spinner)v.findViewById(R.id.sp2);
        sp3=(Spinner)v.findViewById(R.id.sp3);
        sp4=(Spinner)v.findViewById(R.id.sp4);
        return v;
    }

}

